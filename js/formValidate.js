/******f**********
    
    Assignment 6 Javascript
    Name: Anh Tran
    Date: Nov 12, 2019
    Description: Assignment 6

*****************/

const itemDescription = [
  "MacBook",
  "The Razer",
  "WD My Passport",
  "Nexus 7",
  "DD-45 Drums"
];
const itemPrice = [1899.99, 79.99, 179.99, 249.99, 119.99];
const itemImage = [
  "mac.png",
  "mouse.png",
  "wdehd.png",
  "nexus.png",
  "drums.png"
];
const postalRegExp = new RegExp(
  /^[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]$/
);
const emailRegExp = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
let numberOfItemsInCart = 0;
let orderTotal = 0;

/*
 * Handles the submit event of the survey form
 *
 * param e  A reference to the event object
 * return   True if no validation errors; False if the form has
 *          validation errors
 */
function validate(e) {
  if (formHasErrors()) {
    console.log("Error");
    return false;
  }
}

/*
 * Handles the reset event for the form.
 *
 * param e  A reference to the event object
 * return   True allows the reset to happen; False prevents
 *          the browser from resetting the form.
 */
function resetForm(e) {
  // Confirm that the user wants to reset the form.
  if (confirm("Clear order?")) {
    // Ensure all error fields are hidden
		hideErrors();

    // Set focus to the first text field on the page
    document.getElementById("qty1").focus();

    // When using onReset="resetForm()" in markup, returning true will allow
    // the form to reset
    return true;
  }

  // Prevents the form from resetting
  e.preventDefault();

  // When using onReset="resetForm()" in markup, returning false would prevent
  // the form from resetting
  return false;
}

/*
 * Does all the error checking for the form.
 *
 * return   True if an error was found; False if no errors were found
 */
function formHasErrors() {
  let cartTotal = document.getElementById("noItems");
  if (cartTotal.style.display != "none") {
		alert("You have no items in your cart.");
    document.getElementById("qty1").focus();
    return true;
  }
  let postalValue = document.getElementById("postal").value;
  let emailValue = document.getElementById("email").value;
  let expiryMonth = document.getElementById("month").value;
  let expiryYear = document.getElementById("year").value;
  let cardNumber = document.getElementById("cardnumber").value;

	let cardNumberError = checkCardNumber("cardnumber", cardNumber);
  let monthError = checkErrorForField("month");
  let validExpiryDate = checkExpiryDate(expiryMonth, parseInt(expiryYear));
  let cardError = checkErrorForField("cardname");
  let cardTypeValid = checkCardType();
  let emailError = checkErrorForField("email");
	let validEmailError = checkValidFormat("email", emailValue, emailRegExp);

  let postalError = checkErrorForField("postal");
  let validPostalError = checkValidFormat("postal", postalValue, postalRegExp);

  let provinceError = checkErrorForField("province");
  let cityError = checkErrorForField("city");
  let addressError = checkErrorForField("address");
  let nameError = checkErrorForField("fullname");

  return (
    nameError ||
    addressError ||
    cityError ||
    provinceError ||
    postalError ||
    emailError ||
    cardError ||
    monthError ||
    cardNumberError ||
    !cardTypeValid ||
    !validPostalError ||
    !validEmailError ||
    validExpiryDate
  );
}

function checkCardNumber(field, cardNumber) {
  let fieldName = document.getElementById(`${field}`);
  let hasError = false;
  document.getElementById(`${field}_error`).style.display = "none";

  if (fieldName.value == "") {
    document.getElementById(`${field}_error`).style.display = "block";
		hasError = true;
  } else {
		hasError = checkModule(cardNumber);
	}
	
	if (hasError)
	{
		fieldName.focus();
	}
  return hasError;
}

function checkModule(creditNumber) {
  let error = false;
  document.getElementById("invalidcard_error").style.display = "none";
  if (creditNumber.length != 10) {
    error = true;
    document.getElementById("invalidcard_error").style.display = "block";
  } else {
    let checkValid = "432765432";
    let sum = 0;
    for (let i = 0; i < checkValid.length; i++) {
      let testNumber = parseInt(creditNumber.charAt(i));
      let checkNumber = parseInt(checkValid.charAt(i));
      sum += testNumber * checkNumber;
    }
    let checkDigit = 11 - (sum % 11);
    let lastDigit = parseInt(creditNumber.charAt(9));
    if (checkDigit != lastDigit) {
      error = true;
      document.getElementById("invalidcard_error").style.display = "block";
    }
  }
  return error;
}

function checkExpiryDate(month, year) {
  let today = new Date();
  let currentMonth = today.getMonth();
  let currentYear = today.getFullYear();
  document.getElementById("expiry_error").style.display = "none";
  let error = false;

  if (!isNaN(month) && month < currentMonth && currentYear == year) {
    document.getElementById("expiry_error").style.display = "block";
    error = true;
  }
  return error;
}

function checkCardType() {
  let cardTypes = document.getElementsByName("cardtype");
  document.getElementById("cardtype_error").style.display = "block";
  let valid = cardTypes[0].checked || cardTypes[1].checked || cardTypes[2].checked;
  if (valid) {
		document.getElementById("cardtype_error").style.display = "none";
	}
	else
	{
		cardTypes[0].focus();
	}
  return valid;
}

function checkErrorForField(field) {
  let fieldName = document.getElementById(`${field}`);
  let hasError = false;
  document.getElementById(`${field}_error`).style.display = "none";

  if (fieldName.value == "") {
    document.getElementById(`${field}_error`).style.display = "block";
		hasError = true;
		fieldName.focus();
  }

  if (field == "month" && fieldName.value == "- Month -") {
		document.getElementById(`${field}_error`).style.display = "block";
		fieldName.focus();
    hasError = true;
  }
  return hasError;
}

function checkValidFormat(field, code, regexp) {
  if (code.length > 0) {
    let fieldName = document.getElementById(`${field}`);
    let isMatch = regexp.test(code);
    document.getElementById(`${field}format_error`).style.display = "none";
    if (!isMatch) {
			document.getElementById(`${field}format_error`).style.display = "block";
			fieldName.focus();
    }
    return isMatch;
	}
}

/*
 * Adds an item to the cart and hides the quantity and add button for the product being ordered.
 *
 * param itemNumber The number used in the id of the quantity, item and remove button elements.
 */
function addItemToCart(itemNumber) {
  // Get the value of the quantity field for the add button that was clicked
  let quantityValue = trim(document.getElementById("qty" + itemNumber).value);

  // Determine if the quantity value is valid
  if (
    !isNaN(quantityValue) &&
    quantityValue != "" &&
    quantityValue != null &&
    quantityValue != 0 &&
    !document.getElementById("cartItem" + itemNumber)
  ) {
    // Hide the parent of the quantity field being evaluated
    document.getElementById("qty" + itemNumber).parentNode.style.visibility =
      "hidden";

    // Determine if there are no items in the car
    if (numberOfItemsInCart == 0) {
      // Hide the no items in cart list item
      document.getElementById("noItems").style.display = "none";
    }

    // Create the image for the cart item
    let cartItemImage = document.createElement("img");
    cartItemImage.src = "images/" + itemImage[itemNumber - 1];
    cartItemImage.alt = itemDescription[itemNumber - 1];

    // Create the span element containing the item description
    let cartItemDescription = document.createElement("span");
    cartItemDescription.innerHTML = itemDescription[itemNumber - 1];

    // Create the span element containing the quanitity to order
    let cartItemQuanity = document.createElement("span");
    cartItemQuanity.innerHTML = quantityValue;

    // Calculate the subtotal of the item ordered
    let itemTotal = quantityValue * itemPrice[itemNumber - 1];

    // Create the span element containing the subtotal of the item ordered
    let cartItemTotal = document.createElement("span");
    cartItemTotal.innerHTML = formatCurrency(itemTotal);

    // Create the remove button for the cart item
    let cartItemRemoveButton = document.createElement("button");
    cartItemRemoveButton.setAttribute("id", "removeItem" + itemNumber);
    cartItemRemoveButton.setAttribute("type", "button");
    cartItemRemoveButton.innerHTML = "Remove";
    cartItemRemoveButton.addEventListener(
      "click",
      // Annonymous function for the click event of a cart item remove button
      function() {
        // Removes the buttons grandparent (li) from the cart list
        this.parentNode.parentNode.removeChild(this.parentNode);

        // Deteremine the quantity field id for the item being removed from the cart by
        // getting the number at the end of the remove button's id
        let itemQuantityFieldId = "qty" + this.id.charAt(this.id.length - 1);

        // Get a reference to quanitity field of the item being removed form the cart
        let itemQuantityField = document.getElementById(itemQuantityFieldId);

        // Set the visibility of the quantity field's parent (div) to visible
        itemQuantityField.parentNode.style.visibility = "visible";

        // Initialize the quantity field value
        itemQuantityField.value = "";

        // Decrement the number of items in the cart
        numberOfItemsInCart--;

        // Decrement the order total
        orderTotal -= itemTotal;

        // Update the total purchase in the cart
        document.getElementById("cartTotal").innerHTML = formatCurrency(
          orderTotal
        );

        // Determine if there are no items in the car
        if (numberOfItemsInCart == 0) {
          // Show the no items in cart list item
          document.getElementById("noItems").style.display = "block";
        }
      },
      false
    );

    // Create a div used to clear the floats
    let cartClearDiv = document.createElement("div");
    cartClearDiv.setAttribute("class", "clear");

    // Create the paragraph which contains the cart item summary elements
    let cartItemParagraph = document.createElement("p");
    cartItemParagraph.appendChild(cartItemImage);
    cartItemParagraph.appendChild(cartItemDescription);
    cartItemParagraph.appendChild(document.createElement("br"));
    cartItemParagraph.appendChild(document.createTextNode("Quantity: "));
    cartItemParagraph.appendChild(cartItemQuanity);
    cartItemParagraph.appendChild(document.createElement("br"));
    cartItemParagraph.appendChild(document.createTextNode("Total: "));
    cartItemParagraph.appendChild(cartItemTotal);

    // Create the cart list item and add the elements within it
    let cartItem = document.createElement("li");
    cartItem.setAttribute("id", "cartItem" + itemNumber);
    cartItem.appendChild(cartItemParagraph);
    cartItem.appendChild(cartItemRemoveButton);
    cartItem.appendChild(cartClearDiv);

    // Add the cart list item to the top of the list
    let cart = document.getElementById("cart");
    cart.insertBefore(cartItem, cart.childNodes[0]);

    // Increment the number of items in the cart
    numberOfItemsInCart++;

    // Increment the total purchase amount
    orderTotal += itemTotal;

    // Update the total puchase amount in the cart
    document.getElementById("cartTotal").innerHTML = formatCurrency(orderTotal);
  }
}

/*
 * Hides all of the error elements.
 */
function hideErrors() {
  // Get an array of error elements
  let error = document.getElementsByClassName("error");

  // Loop through each element in the error array
  for (let i = 0; i < error.length; i++) {
    // Hide the error element by setting it's display style to "none"
    error[i].style.display = "none";
  }
}

const addItem = e => {
  console.log(this);
  console.log(e);
};

/*
 * Handles the load event of the document.
 */
function load() {
  //	Populate the year select with up to date values
  hideErrors();
  let year = document.getElementById("year");
  let currentDate = new Date();
  for (let i = 0; i < 7; i++) {
    let newYearOption = document.createElement("option");
    newYearOption.value = currentDate.getFullYear() + i;
    newYearOption.innerHTML = currentDate.getFullYear() + i;
    year.appendChild(newYearOption);
  }
  let btnSubmit = document.getElementById("submit");
  let btnReset = document.getElementById("clear");
  let btnAddMac = document.getElementById("addMac");
  let btnAddMouse = document.getElementById("addMouse");
  let btnAddWD = document.getElementById("addWD");
  let btnAddNexus = document.getElementById("addNexus");
	let btnAddDrums = document.getElementById("addDrums");
	let form = document.getElementsByTagName("form")[0];
  btnSubmit.onclick = () => validate();
  btnReset.onclick = () => resetForm();
  btnAddMac.onclick = () => addItemToCart("1");
  btnAddMouse.onclick = () => addItemToCart("2");
  btnAddWD.onclick = () => addItemToCart("3");
  btnAddNexus.onclick = () => addItemToCart("4");
  btnAddDrums.onclick = () => addItemToCart("5");
}

// Add document load event listener
document.addEventListener("DOMContentLoaded", load);
