<?php 
$qty1 = $_POST['qty1'];
$qty2 = $_POST['qty2'];
$qty3 = $_POST['qty3'];
$qty4 = $_POST['qty4'];
$qty5 = $_POST['qty5'];
$fullname = $_POST['fullname'];
$email = $_POST['email'];
$postal = $_POST['postal'];
$cardnumber = $_POST['cardnumber'];
$month = $_POST['month'];
$year = $_POST['year'];
$cardtype = $_POST['cardtype'];
$cardname = $_POST['cardname'];
$address = $_POST['address'];
$city = $_POST['city'];
$province = $_POST['province'];


$itemList = [];
$totalAmount = 0;


if ($qty1 > 0)
{
  array_push($itemList, ["iMac", 1899.99, $qty1 ]);
  $totalAmount += $qty1 * 1899.99;
};
if ($qty2 > 0)
{
  array_push($itemList, ["Razer Mouse", 79.99, $qty2 ]);
  $totalAmount += $qty2 * 79.99;
};
if ($qty3 > 0)
{
  array_push($itemList, ["WD HDD", 179.99, $qty3 ]);
  $totalAmount += $qty3 * 179.99;
};
if ($qty4 > 0)
{
  array_push($itemList, ["Nexus", 249.99, $qty4 ]);
  $totalAmount += $qty4 * 249.99;
};
if ($qty5 > 0)
{
  array_push($itemList, ["Drums", 119.99, $qty5 ]);
  $totalAmount += $qty5 * 119.99;
};

function validation() {
  return fullnameValidation()
  && emailValidation()
  && postalCodeValidation()
  && creditCardNumberValidation()
  && creditCardMonthValidation()
  && creditCardYearValidation()
  && creditCardTypeValidation()
  && fullnameValidation()
  && cardNameValidation()
  && addressValidation()
  && cityValidation()
  && provinceValidation()
  && quantityValidation();
};


function fullnameValidation(){
  global $fullname;
  return  (isset($fullname) && strlen($fullname) > 0);
};

function emailValidation(){
  return filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
};

function postalCodeValidation(){
  return preg_match('/(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]/', $_POST['postal']);
};

function creditCardNumberValidation(){
  global $cardnumber;
  return filter_input(INPUT_POST, 'cardnumber', FILTER_VALIDATE_INT) && strlen((string)$cardnumber) == 10;
};

function creditCardMonthValidation(){
  return filter_input(INPUT_POST, 'month', FILTER_VALIDATE_INT, array("options"=> array("min_range"=>1, "max_range"=>12)));
};

function creditCardYearValidation(){
  return filter_input(INPUT_POST, 'year', FILTER_VALIDATE_INT, array("options"=> array("min_range"=>date("Y")  , "max_range"=>date("Y") + 5)));
};

function creditCardTypeValidation(){
  global $cardtype;
  if ($cardtype &&  ( $cardtype === "VISA" || $cardtype === "AmEx" || $cardtype === "Mastercard")) {
    return true;
  }
  return false;
};

function cardNameValidation(){
  global $cardname;
  return  (isset($cardname) && strlen($cardname) > 0);
};

function addressValidation(){
  global $address;
  return  (isset($address) && strlen($address) > 0);
};

function cityValidation(){
  global $city;
  return  (isset($city) && strlen($city) > 0);
};
function provinceValidation(){
  return preg_match('/(N[BLSTU]|[AMN]B|[BQ]C|ON|PE|SK)/', $_POST['province']);
};
function quantityValidation(){
  global $qty1, $qty2, $qty3, $qty4, $qty5;

  if ($qty1 != NULL && (!filter_input(INPUT_POST, 'qty1', FILTER_VALIDATE_INT))) {
    return false;
  };
  if ($qty2 != NULL && (!filter_input(INPUT_POST, 'qty2', FILTER_VALIDATE_INT))) {
     return false;
  };
  if ($qty3 != NULL && (!filter_input(INPUT_POST, 'qty3', FILTER_VALIDATE_INT))) {
    return false;
  };
  if ($qty4 != NULL && (!filter_input(INPUT_POST, 'qty4', FILTER_VALIDATE_INT))) {
    return false;
  };
  if ($qty5 != NULL && (!filter_input(INPUT_POST, 'qty5', FILTER_VALIDATE_INT))) {
    return false;
  };
  return true;
};

?>

<!doctype html>
<html lang="en">
<?php if(!validation()){ ?>

<head>
  <title>Validation error</title>
  <link rel="stylesheet" type="text/css" href="invoice.css" />
</head>

<body>
  <div class="invoice">
    <h2>Please input valid data!</h2>
  </div>
</body>
<?php } else { ?>

<head>
  <title>Thanks for shopping at the WebDev Store!</title>
  <link rel="stylesheet" type="text/css" href="invoice.css" />
</head>

<body>
  <div class="invoice">
    <h2>Thanks for your order <?=$fullname?>.</h2>
    <h3>Here's a summary of your order:</h3>
    <table>
      <tr>
        <td colspan="4">
          <h3>Address Information</h3>
        </td>
      </tr>
      <tr>
        <td class="alignright"><span class="bold">Address:</span>
        </td>
        <td><?=$address?></td>
        <td class="alignright"><span class="bold">City:</span>
        </td>
        <td><?=$city?></td>
      </tr>
      <tr>
        <td class="alignright"><span class="bold">Province:</span>
        </td>
        <td><?=$province?> </td>
        <td class="alignright"><span class="bold">Postal Code:</span>
        </td>
        <td><?=$postal?> </td>
      </tr>
      <tr>
        <td colspan="2" class="alignright"><span class="bold">Email:</span>
        </td>
        <td colspan="2"><?=$email?></td>
      </tr>
    </table>

    <table>
      <tr>
        <td colspan="3">
          <h3>Order Information</h3>
        </td>
      </tr>
      <tr>
        <td><span class="bold">Quantity</span>
        </td>
        <td><span class="bold">Description</span>
        </td>
        <td><span class="bold">Cost</span>
        </td>
      </tr>
      <?php forEach($itemList as $item): ?>
      <tr>
        <td><?=$item[2]?></td>
        <td><?=$item[0]?></td>
        <td class='alignright'><?=$item[1] * $item[2]?></td>
      </tr>
      <?php endforEach;?>
      <tr>
        <td colspan="2" class='alignright'><span class="bold">Totals</span></td>
        <td class='alignright'>
          <span class='bold'>$ <?=$totalAmount?></span> </td>
      </tr>
    </table>
  </div>
  <?php if ($totalAmount > 20000): ?>
  <div id="rollingrick">
    <h2>Congrats on the big order. Rick Astley congratulates you.</h2>
    <iframe width="600" height="475" src="//www.youtube.com/embed/dQw4w9WgXcQ" frameborder="0" allowfullscreen></iframe>
  </div>
  <?php endif; ?>
</body>
<?php } ?>

</html>